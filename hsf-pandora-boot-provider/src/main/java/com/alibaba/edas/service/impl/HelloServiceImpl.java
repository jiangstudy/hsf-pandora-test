package com.alibaba.edas.service.impl;

import com.alibaba.boot.hsf.annotation.HSFProvider;
import com.alibaba.edas.service.HelloService;


@HSFProvider(serviceInterface = HelloService.class, serviceVersion = "1.0.0")
public class HelloServiceImpl implements HelloService {
    @Override
    public String echo(String string) {
        try {
            Thread.sleep(4000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return string;
    }
}
