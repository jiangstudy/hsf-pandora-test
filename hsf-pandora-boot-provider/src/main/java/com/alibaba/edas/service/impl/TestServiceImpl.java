package com.alibaba.edas.service.impl;

import com.alibaba.boot.hsf.annotation.HSFProvider;
import com.alibaba.edas.service.TestService;

@HSFProvider(serviceInterface = TestService.class)
public class TestServiceImpl implements TestService {
    @Override
    public String test1() {
        return "test1";
    }

    @Override
    public int test2() {
        return 12138;
    }
}
