package com.alibaba.edas.service.impl;

import com.alibaba.boot.hsf.annotation.HSFProvider;
import com.alibaba.edas.service.AsynHelloService;

@HSFProvider(serviceInterface = AsynHelloService.class)
public class AsynHelloServiceImpl implements AsynHelloService {
    @Override
    public String asynHello(String s) {
        System.out.println("enter asynHello");

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            System.out.println("sleep exception");
//        }

        System.out.println("before return");
        return "asyn " + s;

    }
}
