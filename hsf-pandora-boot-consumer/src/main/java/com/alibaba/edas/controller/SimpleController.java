package com.alibaba.edas.controller;


import com.alibaba.edas.service.AsynHelloService;
import com.alibaba.edas.service.HelloService;
import com.alibaba.edas.service.TestService;
import com.taobao.hsf.tbremoting.invoke.HSFFuture;
import com.taobao.hsf.tbremoting.invoke.HSFResponseFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class SimpleController {


    @Autowired
    private HelloService helloService;

    @Autowired
    private TestService testService;

    @Autowired
    private AsynHelloService asynHelloService;

    @GetMapping(value = "/hello/{str}")
    public String echo(@PathVariable String str) {
        return helloService.echo(str);
    }

    @GetMapping("/test1")
    public String test1() {
        return testService.test1();
    }

    @GetMapping("/test2")
    public Integer test2() {
        return testService.test2();
    }

    @GetMapping("/asynHello/{s}")
    public Object test3(@PathVariable String s) {
        System.out.println("enter simple controller ---   asynHello method");


        String ans = asynHelloService.asynHello(s);

        HSFFuture hsfFuture = HSFResponseFuture.getFuture();

        System.out.println("asyn ans " + ans);

        try {
            ans = (String) hsfFuture.getResponse(5000);
            System.out.println("ans is " + ans);
        } catch (Exception e) {
            System.out.println("异步调用异常");
            e.printStackTrace();
        } catch (Throwable t) {
            System.out.println("throwable");
            System.out.println(t);
        }
        System.out.println("after 异步调用");
        return ans == null ? "null" : ans;

    }
}
