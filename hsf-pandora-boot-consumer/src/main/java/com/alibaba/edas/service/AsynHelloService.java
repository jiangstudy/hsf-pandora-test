package com.alibaba.edas.service;

public interface AsynHelloService {

    String asynHello(String s);
}
