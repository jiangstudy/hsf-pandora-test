package com.alibaba.edas.config;

import com.alibaba.boot.hsf.annotation.HSFConsumer;

import com.alibaba.edas.service.AsynHelloService;
import com.alibaba.edas.service.HelloService;
import com.alibaba.edas.service.TestService;
import org.springframework.context.annotation.Configuration;


@Configuration
public class HsfConfig {

    @HSFConsumer(clientTimeout = 3000, serviceVersion = "1.0.0")
    private HelloService helloService;

    @HSFConsumer()
    private TestService testService;

    @HSFConsumer(futureMethods = "asynHello")
    private AsynHelloService asynHelloService;

}
